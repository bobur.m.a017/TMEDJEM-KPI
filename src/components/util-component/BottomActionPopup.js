import React from "react";

const BottomActionPopup = ({ open, children }) => {
  return (
    <div className={`customer-card-bottom-actions ${open ? "open" : ""}`}>
      {children}
    </div>
  );
};

export default BottomActionPopup;
