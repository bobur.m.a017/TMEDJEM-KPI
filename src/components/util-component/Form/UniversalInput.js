import React from "react";
import { Form, Input } from "antd";
import { tl } from "i18n/i18n";

const UniversalInput = ({
	label,
	name,
	rules,
	restField,
	fieldKey,
	suffix,
	placeholder,
	className,
	size,
	type = "text",
}) => {
	return (
		<Form.Item
			{...restField}
			label={label}
			name={name}
			rules={rules}
			fieldKey={fieldKey}
		>
			<Input
				type={type}
				size={size}
				suffix={suffix}
				placeholder={tl(placeholder)}
				className={className}
			/>
		</Form.Item>
	);
};

export default UniversalInput;
