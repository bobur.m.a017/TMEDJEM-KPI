import React from "react";
import { Form, Select } from "antd";

const { Option } = Select;

const InputSelect = ({
  label,
  name,
  rules,
  placeholder,
  options = [],
  mode = "",
  restField,
  fieldKey,
  size,
  className,
  defaultValue,
  allowClear,
  disabled = false,
}) => {
  return (
    <Form.Item
      {...restField}
      label={label}
      name={name}
      rules={rules}
      fieldKey={fieldKey}
      initialValue={defaultValue}
    >
      <Select
        allowClear={allowClear}
        className={className}
        size={size}
        showSearch
        mode={mode}
        disabled={disabled}
        filterOption={(input, option) =>
          option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }
        // filterSort={(optionA, optionB) =>
        //   optionA.children
        //     .toLowerCase()
        //     .localeCompare(optionB.children.toLowerCase())
        // }
        placeholder={placeholder}
      >
        {options.map((o, i) => (
          <Option key={i} value={o.id ? o.id : o.value}>
            {o.name}
          </Option>
        ))}
      </Select>
    </Form.Item>
  );
};

export default InputSelect;
