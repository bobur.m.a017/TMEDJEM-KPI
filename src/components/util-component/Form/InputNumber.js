import React from "react";
import { Form, Input } from "antd";

const InputNum = ({
  label,
  name,
  rules,
  restField,
  fieldKey,
  suffix = "",
  placeholder,
  disabled,
  validateStatus,
  help,
}) => {
  return (
    <Form.Item
      rules={rules}
      {...restField}
      label={label}
      name={name}
      fieldKey={fieldKey}
      validateStatus={validateStatus}
      help={help}
    >
      <Input
        disabled={disabled}
        placeholder={placeholder}
        type="number"
        suffix={suffix}
      />
    </Form.Item>
  );
};

export default InputNum;
