import { Card, Row } from "antd";
import React from "react";

function CustomCardBlock({ children, title }) {
  return (
    <div>
      <Card
        title={
          <Row justify="space-between" align="center">
            <h4 className="customer-card-title">{title}</h4>
          </Row>
        }
      >
        {children}
      </Card>
    </div>
  );
}

export default CustomCardBlock;
