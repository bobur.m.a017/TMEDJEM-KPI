import React from "react";
import { Spin } from "antd";
import { LoadingOutlined } from "@ant-design/icons";

const Icon = <LoadingOutlined style={{ fontSize: 35 }} spin />;

const Loading = () => {
  return (
    <div className={`loading text-center`}>
      <Spin indicator={Icon} />
    </div>
  );
};

export default Loading;
