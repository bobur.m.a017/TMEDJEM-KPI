import React from "react";
import { Table } from "antd";
import ToolBar from "./components/ToolBar";

function TableView({
  title = "",
  columns,
  data,
  filter,
  className = "",
  expandable,
  hidden = false,
  loading,
  sticky = false,
  name,
  id,
}) {
  return (
    <div>
      <ToolBar hidden={hidden} title={title} filter={filter} name={name} />
      <Table
        id={id}
        expandable={expandable}
        className={className}
        columns={columns}
        dataSource={data}
        loading={loading}
        scroll={{ x: 1500 }}
        sticky={sticky}
      />
    </div>
  );
}

export default TableView;
