import React, { useContext, useState } from "react";
import { Row, Col, Button } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import { MainContext } from "../../../context/MainContext";
import SearchFilter from "./Search&Filter";
function ToolBar({ title, filter, hidden, name }) {
  const { setVisible, toggleFilter, setToggleFilter } = useContext(MainContext);
  return (
    <div className="tool-bar">
      <div
        onClick={() => setToggleFilter(false)}
        className={toggleFilter ? "dark-overlay" : ""}
      ></div>
      <Row>
        <Col span={6}>
          <div style={{ fontSize: "22px" }}>{title}</div>
        </Col>
        <Col span={12}>
          {filter && (
            <SearchFilter
              setActive={setToggleFilter}
              active={toggleFilter}
              name={name}
            />
          )}
        </Col>
        <Col
          span={6}
          style={{
            display: "flex",
            justifyContent: "flex-end",
          }}
        >
          <Button
            className={hidden ? "d-none" : "flex"}
            onClick={() => setVisible(true)}
            type="primary"
            icon={<PlusOutlined />}
          >
            Qo'shish
          </Button>
        </Col>
      </Row>
    </div>
  );
}

export default ToolBar;
