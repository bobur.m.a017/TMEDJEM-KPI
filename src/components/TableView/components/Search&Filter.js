import React from "react";
import ColculationFilter from "../../../view/app-view/colculation/components/filter";
import FilterStuff from "../../../view/app-view/staff/components/filter";

function SearchFilter({ setActive, active, name }) {
  return (
    <>
      {name === "staff" && (
        <FilterStuff setActive={setActive} active={active} />
      )}
      {name === "colculation" && (
        <ColculationFilter setActive={setActive} active={active} />
      )}
    </>
  );
}

export default SearchFilter;
