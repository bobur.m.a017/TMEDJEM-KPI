import { Form } from "antd";
import React, { useEffect, useState } from "react";
import { CategoryApi } from "../api/main/category";
import { PositionApi } from "../api/main/position";
import { ReportApi } from "../api/main/report";
import { UserApi } from "../api/main/user";

export const MainContext = React.createContext();

const MainContextProvider = ({ children }) => {
  const [form] = Form.useForm();
  const [visible, setVisible] = useState(false);
  const [toggleFilter, setToggleFilter] = useState(false);
  const [edit, setEdit] = useState(false);
  const [loader, setLoader] = useState(true);
  const [position, setPosition] = useState([]);
  const [category, setCategory] = useState([]);
  const [currentData, setCurrentData] = useState(null);
  const [userList, setUserList] = useState([]);
  const [managemenSub, setManagementSub] = useState([]);
  const [categorySub, setCategorySub] = useState([]);
  const [reportList, setReportList] = useState([]);

  const getUser = (params) => {
    if (localStorage.getItem("id") === "1") {
      UserApi.getAll(params)
        .then((response) => {
          setUserList(response);
          setToggleFilter(false);
          setLoader(false);
        })
        .catch((error) => {
          console.log(error);
          setLoader(false);
        });
    }
  };
  const getReport = (params, setActive = () => {}) => {
    ReportApi.getAll(params)
      .then((res) => {
        setReportList(res);
        setActive(false);
        setLoader(false);
      })
      .catch((error) => {
        console.log(error);
        setLoader(false);
      });
  };
  const dataFetch = () => {
    if (localStorage.getItem("id") === "1") {
      PositionApi.getAll()
        .then((response) => {
          setLoader(false);
          setPosition(response);
        })
        .catch((error) => {
          console.log(error);
          setLoader(false);
        });
      CategoryApi.getAll()
        .then((response) => {
          setLoader(false);
          setCategory(response);
        })
        .catch((error) => {
          console.log(error);
          setLoader(false);
        });
    }
  };
  const resetFields = () => {
    setVisible(false);
    form.resetFields();
    setManagementSub([]);
    setCategorySub([]);
  };
  useEffect(() => {
    dataFetch();
  }, []);
  return (
    <MainContext.Provider
      value={{
        form,
        visible,
        setVisible,
        edit,
        setEdit,
        position,
        category,
        dataFetch,
        resetFields,
        currentData,
        setCurrentData,
        userList,
        getUser,
        toggleFilter,
        setToggleFilter,
        managemenSub,
        setManagementSub,
        categorySub,
        setCategorySub,
        reportList,
        getReport,
        loader,
        setLoader,
      }}
    >
      {children}
    </MainContext.Provider>
  );
};

export default MainContextProvider;
