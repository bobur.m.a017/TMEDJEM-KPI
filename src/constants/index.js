export const StuffStatus = [
  { value: "Ishlamoqda", name: "Ishlamoqda" },
  { value: "Tatilda", name: "Ta'tilda" },
];

export const CategoryOptions = [
  { value: "management", name: "Rahbariyat" },
  { value: "boards", name: "Boshqarma" },
  { value: "section", name: "Bo'lim" },
];
export const month = [
  { value: 1, name: "Yanvar" },
  { value: 2, name: "Fevral" },
  { value: 3, name: "Mart" },
  { value: 4, name: "Aprel" },
  { value: 5, name: "May" },
  { value: 6, name: "Iyun" },
  { value: 7, name: "Iyul" },
  { value: 8, name: "Avgust" },
  { value: 9, name: "Sentabr" },
  { value: 10, name: "Oktabr" },
  { value: 11, name: "Noyabr" },
  { value: 12, name: "Dekabr" },
];

export const year = [
  { value: 2019, name: "2019 - yil" },
  { value: 2020, name: "2020 - yil" },
  { value: 2021, name: "2021 - yil" },
  { value: 2022, name: "2022 - yil" },
  { value: 2023, name: "2023 - yil" },
  { value: 2024, name: "2024 - yil" },
  { value: 2025, name: "2025 - yil" },
  { value: 2026, name: "2026 - yil" },
  { value: 2027, name: "2027 - yil" },
  { value: 2028, name: "2028 - yil" },
  { value: 2029, name: "2029 - yil" },
  { value: 2030, name: "2030 - yil" },
  { value: 2031, name: "2031 - yil" },
  { value: 2032, name: "2032 - yil" },
  { value: 2033, name: "2033 - yil" },
  { value: 2034, name: "2034 - yil" },
  { value: 2035, name: "2035 - yil" },
  { value: 2036, name: "2036 - yil" },
  { value: 2037, name: "2037 - yil" },
];
