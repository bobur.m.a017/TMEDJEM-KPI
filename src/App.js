import React from "react";
import Views from "./view";
function App() {
  return (
    <div className="App">
      <Views />
    </div>
  );
}

export default App;
