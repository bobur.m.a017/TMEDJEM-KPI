import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import MainLayout from "../layout";
import LoginTwo from "./login";

const Views = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={LoginTwo} />
        <Route path="/app" component={MainLayout} />
      </Switch>
    </Router>
  );
};

export default Views;
