import React, { useState } from "react";
import { Form, Input, Button, message } from "antd";
import axios from "axios";
import { useHistory } from "react-router-dom";
export const LoginForm = () => {
  const [loading, setLoading] = useState(false);

  const history = useHistory();

  const onLogin = async (values) => {
    setLoading(true);
    await axios
      .post("https://kpi.edu.uz/kpi/api/user/signIn", values)
      .then((response) => {
        localStorage.setItem("name", response.data.name);
        localStorage.setItem("lastName", response.data.lastName);
        localStorage.setItem("token", response.data.token);
        localStorage.setItem("role", response.data.role);
        localStorage.setItem("id", response.data.id);
        if (response.data.role === "ROLE_Admin") {
          history.push("/app/management");
        } else if (response.data.role === "ROLE_O`rinbosar") {
          history.push("/app/colculation");
        } else if (response.data.role === "ROLE_Buxgalter") {
          history.push("/app/management");
        } else if (response.data.role === "ROLE_Nazorat") {
          history.push("/app/colculation");
        }
        message.success("Success");
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
        console.error(error);
        message.error("Login yoki parol noto'g'ri!");
      });
  };

  return (
    <>
      <Form id="myform" layout="vertical" name="login-form" onFinish={onLogin}>
        <Form.Item
          name="login"
          label="Login"
          rules={[
            {
              required: true,
              message: "validation.sign-in.input-your-usern",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="password"
          label="Parol"
          rules={[
            {
              required: true,
              message: "validation.sign-in.input-your-password",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item>
          <Button
            loading={loading}
            type="primary"
            htmlType="submit"
            block
            loading={loading}
          >
            Login
          </Button>
        </Form.Item>
      </Form>
    </>
  );
};

export default LoginForm;
