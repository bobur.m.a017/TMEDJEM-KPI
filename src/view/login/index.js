import React from "react";
import LoginForm from "./components/LoginForm";
import { Row, Col } from "antd";
import Bg from "../../assets/images/img.png";
import Bg1 from "../../assets/images/img-17.jpg";
import Logo from "../../assets/images/logo.png";

const backgroundStyle = {
  backgroundImage: `url(${Bg1})`,
  backgroundRepeat: "no-repeat",
  backgroundSize: "cover",
};

const LoginTwo = () => {
  return (
    <div className="h-100" style={{ minHeight: "100vh" }}>
      <Row
        justify="center"
        className="align-items-stretch h-100"
        style={{ minHeight: "100vh" }}
      >
        <Col xs={20} sm={20} md={24} lg={16}>
          <div className="container d-flex flex-column justify-content-center h-100">
            <Row justify="center">
              <Col
                xs={0}
                sm={0}
                md={0}
                lg={20}
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  flexDirection: "column",
                }}
              >
                <img
                  style={{ display: "flex", width: "150px" }}
                  className="img-fluid mb-2"
                  src={Logo}
                  alt=""
                />
                <p
                  style={{
                    textAlign: "center",
                    fontWeight: "700",
                  }}
                >
                  O'ZBEKISTON RESPUBLIKASI <br /> OLIY VA O'RTA MAXSUS TA'LIM
                  VAZIRLIGI
                </p>
              </Col>
              <Col xs={24} sm={24} md={20} lg={12} xl={8}>
                <div className="mt-4">
                  <LoginForm />
                </div>
              </Col>
            </Row>
          </div>
        </Col>
        <Col xs={0} sm={0} md={0} lg={8}>
          <div
            className="d-flex flex-column justify-content-between h-100 px-4 pt-5"
            style={backgroundStyle}
          >
            <Row justify="center">
              <Col xs={0} sm={0} md={0} lg={20}>
                <img className="img-fluid mb-5" src={Bg} alt="" />
                <h1 className="text-white">
                  KPI axborot tizimiga <br /> Xush kelibsiz!
                </h1>
                <p className="text-white">
                  Oliy va o'rta maxsus ta'lim vazirligi rahbar xodimlarini ish
                  samaradorligini baholovchi KPI axborot tizimi
                </p>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default LoginTwo;
