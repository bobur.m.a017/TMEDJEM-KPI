import React, { useContext } from "react";
import TableView from "../../../components/TableView";
import { DeleteOutlined } from "@ant-design/icons";
import { message, Popconfirm, Space, Tag } from "antd";
import { MainContext } from "../../../context/MainContext";
import BoardsCard from "./components/BoardsCard";
import { CategoryApi } from "../../../api/main/category";

function Boards() {
  const { setVisible, setEdit, category, dataFetch, form, loader } =
    useContext(MainContext);

  const onClose = () => {
    setVisible(false);
    setEdit(false);
    form.resetFields();
  };
  const handleChange = (value) => {
    form.setFieldsValue({ ...value });
    setEdit(true);
    setVisible(true);
  };

  const Delete = (id) => {
    CategoryApi.delete(id)
      .then((res) => {
        dataFetch();
        message.success(res.text);
      })
      .catch((err) => {
        message.error("error");
        console.log(err);
      });
  };
  const data = category?.filter((data) => data.typeName === "Boshqarma");
  const columns = [
    {
      title: "T/R",
      dataIndex: "id",
      key: "id",
      render: (_, value) => data.indexOf(value) + 1,
    },
    {
      title: "Nomi",
      dataIndex: "name",
      key: "name",
      render: (text, value) => (
        <a onClick={() => handleChange(value)}>{text}</a>
      ),
    },
    {
      title: "Rahbariyat",
      dataIndex: "parentName",
      key: "parentName",
    },
    {
      title: "O'chirish",
      key: "action",
      render: (_, record) => (
        <Popconfirm title="Sure to delete?" onConfirm={() => Delete(record.id)}>
          <Tag color="red" key={0}>
            <DeleteOutlined />
          </Tag>
        </Popconfirm>
      ),
    },
  ];
  return (
    <div>
      <TableView
        title="Boshqarma shakllantirish!"
        columns={columns}
        data={data}
        loading={loader}
      />
      <BoardsCard onClose={onClose} title="Boshqarma qo'shish" />
    </div>
  );
}

export default Boards;
