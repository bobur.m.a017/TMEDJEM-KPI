import React, { useContext } from "react";
import TableView from "../../../components/TableView";
import { DeleteOutlined } from "@ant-design/icons";
import { message, Popconfirm, Space, Tag } from "antd";
import { MainContext } from "../../../context/MainContext";
import SectionCard from "./components/SectionCard";
import { CategoryApi } from "../../../api/main/category";

function Sections() {
  const { setVisible, setEdit, category, dataFetch, form, loader } =
    useContext(MainContext);
  const data = category?.filter((data) => data.typeName === "Bo'lim");
  const onClose = () => {
    setVisible(false);
    setEdit(false);
    form.resetFields();
  };
  const handleChange = (value) => {
    form.setFieldsValue({ ...value });
    setEdit(true);
    setVisible(true);
  };

  const Delete = (id) => {
    CategoryApi.delete(id)
      .then((res) => {
        dataFetch();
        message.success(res.text);
      })
      .catch((err) => {
        message.error("error");
        console.log(err);
      });
  };
  const columns = [
    {
      title: "T/R",
      dataIndex: "id",
      key: "id",
      render: (_, value) => data.indexOf(value) + 1,
    },
    {
      title: "Nomi",
      dataIndex: "name",
      key: "name",
      render: (text, value) => (
        <a onClick={() => handleChange(value)}>{text}</a>
      ),
    },
    {
      title: "Boshqarma",
      dataIndex: "parentName",
      key: "parentName",
    },
    {
      title: "O'chirish",
      key: "action",
      render: (_, record) => (
        <Popconfirm title="Sure to delete?" onConfirm={() => Delete(record.id)}>
          <Tag color="red" key={0}>
            <DeleteOutlined />
          </Tag>
        </Popconfirm>
      ),
    },
  ];
  return (
    <div>
      <TableView
        title="Boshqarma shakllantirish!"
        columns={columns}
        data={data}
        loading={loader}
      />
      <SectionCard onClose={onClose} title="Bo'lim qo'shish" />
    </div>
  );
}

export default Sections;
