import React, { useContext, useEffect, useState } from "react";
import { Drawer } from "antd";
import EditForm from "./EditForm";
import AddFrom from "./AddFrom";
import { MainContext } from "../../../../context/MainContext";

function SectionCard({ onClose, title = "" }) {
  const { edit, visible } = useContext(MainContext);

  return (
    <Drawer
      title={`kpi.edu.uz`}
      placement="right"
      size="large"
      onClose={onClose}
      visible={visible}
    >
      {edit && <EditForm title={title} />}
      {!edit && <AddFrom title={title} />}
    </Drawer>
  );
}
export default SectionCard;
