import { Button, message, Row } from "antd";
import { Form } from "antd";
import React, { useContext } from "react";
import { CategoryApi } from "../../../../api/main/category";
import CustomCardBlock from "../../../../components/Card/CustomCardBlock";
import BottomActionPopup from "../../../../components/util-component/BottomActionPopup";
import InputText from "../../../../components/util-component/Form/InputText";
import InputSelect from "../../../../components/util-component/Form/Select";
import { MainContext } from "../../../../context/MainContext";

function EditForm() {
  const { dataFetch, form, resetFields, category } = useContext(MainContext);

  const onFinish = (value) => {
    CategoryApi.update(form.getFieldValue().id, value)
      .then((res) => {
        dataFetch();
        resetFields();
        message.success(res.text);
      })
      .catch((error) => {
        message.error("Error");
        console.log(error);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <>
      <Form
        form={form}
        initialValues={{}}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <CustomCardBlock title="Boshqarma Tahrirlash">
          <InputText
            name="name"
            placeholder="Boshqarma nomi"
            rules={[
              {
                required: true,
              },
            ]}
          />
          <InputSelect
            name="parentId"
            placeholder="Rahbariyatni tanlang"
            options={category?.filter((data) => data.typeName === "Boshqarma")}
            rules={[
              {
                required: true,
              },
            ]}
          />
        </CustomCardBlock>
        <BottomActionPopup open={true}>
          <Row justify="space-between" align="middle">
            <Button type="primary" htmlType="reset" onClick={resetFields}>
              Bekor qilish
            </Button>
            <Button type="primary" htmlType="submit">
              Saqlash
            </Button>
          </Row>
        </BottomActionPopup>
      </Form>
    </>
  );
}

export default EditForm;
