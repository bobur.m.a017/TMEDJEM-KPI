import { Button, message, Row } from "antd";
import { Form } from "antd";
import React, { useContext, useState } from "react";
import { ReportApi } from "../../../../api/main/report";
import CustomCardBlock from "../../../../components/Card/CustomCardBlock";
import BottomActionPopup from "../../../../components/util-component/BottomActionPopup";
import InputNum from "../../../../components/util-component/Form/InputNumber";
import { MainContext } from "../../../../context/MainContext";

function AddReport({ fetchReportData, form }) {
  const { resetFields, getReport } = useContext(MainContext);
  const [loading, setLoading] = useState(false);
  const [disabled, setDisabled] = useState(true);
  const [disabledButton, setDisabledButton] = useState(true);
  const [value, setValue] = useState(0);
  const [error, setError] = useState(false);

  let sentData = {
    tasks: [{ implementation: "", plan: "", taskId: "" }],
    userId: "",
  };
  const currentUserId = localStorage.getItem("id");
  sentData.userId = form.getFieldValue().id;
  if (form.getFieldValue().report) {
    form
      .getFieldValue()
      .report.userTasks.filter(
        (data) => data.roleId === parseInt(currentUserId)
      )
      .map((data) => {
        sentData.tasks[0].implementation = data.implementation;
        sentData.tasks[0].plan = data.plan;
        sentData.tasks[0].taskId = data.taskId;
      });
  }
  const onFinish = (values) => {
    setLoading(true);
    sentData.tasks[0].implementation = parseInt(values.implementation);
    sentData.tasks[0].plan = parseInt(values.plan);
    if (form.getFieldValue().report?.id) {
      ReportApi.update(form.getFieldValue().report?.id, sentData)
        .then((res) => {
          resetFields();
          message.success(res.text);
          fetchReportData();
          getReport();
          setLoading(false);
        })
        .catch((error) => {
          message.error("Error");
          console.log(error);
          setLoading(false);
        });
    } else {
      ReportApi.create(sentData)
        .then((res) => {
          resetFields();
          message.success(res.text);
          fetchReportData();
          getReport();
          setLoading(false);
        })
        .catch((error) => {
          message.error("Error");
          console.log(error);
          setLoading(false);
        });
    }
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const handleChanged = (field) => {
    if (field[0].name[0] === "plan") {
      setDisabled(false);
      setValue(field[0].value);
    }
    if (field[0].name[0] === "implementation") {
      if (parseInt(field[0].value) > parseInt(value)) {
        setError(true);
        setDisabledButton(true);
      } else {
        setError(false);
        setDisabledButton(false);
      }
    }
  };
  return (
    <Form
      form={form}
      initialValues={{}}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      onFieldsChange={(value) => handleChanged(value)}
    >
      <CustomCardBlock
        title={
          form.getFieldValue()?.name + " " + form.getFieldValue()?.lastName
        }
      >
        <InputNum
          name="plan"
          placeholder="Reja"
          rules={[
            {
              required: true,
            },
          ]}
        />
        <InputNum
          name="implementation"
          placeholder="Bajarilgan"
          rules={[
            {
              required: true,
            },
          ]}
          disabled={disabled}
          validateStatus={error ? "error" : ""}
          help={error ? `${value} dan oshmasligi kerak` : ""}
        />
      </CustomCardBlock>
      <BottomActionPopup open={true}>
        <Row justify="space-between" align="middle">
          <Button
            disabled={disabledButton}
            loading={loading}
            type="primary"
            htmlType="submit"
          >
            Saqlash
          </Button>
        </Row>
      </BottomActionPopup>
    </Form>
  );
}

export default AddReport;
