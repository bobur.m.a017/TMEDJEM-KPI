import React, { useContext } from "react";
import { Drawer } from "antd";
import { MainContext } from "../../../../context/MainContext";
import AddReport from "./AddReport";
import AddReport1 from "./AddReport1";
function ColculationCard({
  onClose,
  fetchReportData,
  userTask,
  taskList,
  form,
}) {
  const { visible } = useContext(MainContext);
  const currentUserId = localStorage.getItem("id");
  return (
    <Drawer
      title={`kpi.edu.uz`}
      placement="right"
      size="large"
      onClose={onClose}
      visible={visible}
    >
      {parseInt(currentUserId) === 4 && (
        <AddReport
          form={form}
          userTask={userTask}
          fetchReportData={fetchReportData}
        />
      )}
      {parseInt(currentUserId) === 6 && (
        <AddReport1
          form={form}
          taskList={taskList}
          userTask={userTask}
          fetchReportData={fetchReportData}
        />
      )}
    </Drawer>
  );
}
export default ColculationCard;
