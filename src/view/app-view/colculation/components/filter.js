import React, { useContext } from "react";
import InputText from "../../../../components/util-component/Form/InputText";
import InputSelect from "../../../../components/util-component/Form/Select";
import { Button, Col, Form, Row, Tooltip } from "antd";
import { SearchOutlined } from "@ant-design/icons";
import { month, year } from "../../../../constants";
import { MainContext } from "../../../../context/MainContext";

function Filter({ setActive, active }) {
  const { getReport } = useContext(MainContext);

  const [form] = Form.useForm();
  const onFinish = (values) => {
    getReport(values, setActive);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const resetEvent = () => {
    form.resetFields();
    setActive(false);
    getReport();
  };
  return (
    <div className={active ? "custom-collapse active" : "custom-collapse"}>
      <Form
        form={form}
        name="basic"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Row gutter={24}>
          <Col span={24}>
            <InputText
              onFocus={() => setActive(!active)}
              className="search-input"
              suffix={
                <Tooltip title="Search">
                  <SearchOutlined />
                </Tooltip>
              }
              placeholder="Search & Filter"
            />
          </Col>
          <Col span={22} offset={1}>
            <InputSelect name="month" placeholder="Oy" options={month} />
          </Col>
          <Col span={22} offset={1}>
            <InputSelect name="year" placeholder="Yil" options={year} />
          </Col>
          <Col
            span={22}
            offset={1}
            style={{ display: "flex", justifyContent: "space-between" }}
          >
            <Form.Item>
              <Button type="primary" htmlType="reset" onClick={resetEvent}>
                Bekor qilish
              </Button>
            </Form.Item>
            <Form.Item>
              <Button loading={!active} type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </div>
  );
}

export default Filter;
