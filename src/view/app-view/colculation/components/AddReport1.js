import { Button, message, Row } from "antd";
import { Form } from "antd";
import React, { useContext, useState } from "react";
import { ReportApi } from "../../../../api/main/report";
import CustomCardBlock from "../../../../components/Card/CustomCardBlock";
import BottomActionPopup from "../../../../components/util-component/BottomActionPopup";
import InputNum from "../../../../components/util-component/Form/InputNumber";
import { MainContext } from "../../../../context/MainContext";
function AddReport1({ fetchReportData, userTask, taskList, form }) {
  const { resetFields } = useContext(MainContext);
  const [loading, setLoading] = useState(false);
  let sentData = {
    tasks: [],
    userId: "",
  };
  const currentData = form.getFieldValue();
  const currentUserId = localStorage.getItem("id");
  sentData.userId = currentData.id;
  taskList
    .filter((data) => data.roleId === parseInt(currentUserId))
    .map((data) => {
      sentData.tasks.push({ value: "", taskId: data.id });
    });
  const onFinish = (values) => {
    setLoading(true);
    sentData.tasks.map((data) => {
      data.value = values[data.taskId];
    });
    if (currentData.report?.id) {
      ReportApi.update(currentData.report?.id, sentData)
        .then((res) => {
          resetFields();
          message.success(res.text);
          fetchReportData();
          setLoading(false);
        })
        .catch((error) => {
          message.error("Error");
          console.log(error);
          setLoading(false);
        });
    } else {
      ReportApi.create(sentData)
        .then((res) => {
          resetFields();
          message.success(res.text);
          fetchReportData();
          setLoading(false);
        })
        .catch((error) => {
          message.error("Error");
          console.log(error);
          setLoading(false);
        });
    }
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <>
      <Form
        form={form}
        initialValues={{ ...userTask }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <CustomCardBlock
          title={
            form.getFieldValue()?.name + " " + form.getFieldValue()?.lastName
          }
        >
          {taskList
            .filter((data) => data.roleId === parseInt(currentUserId))
            .map((data, key) => {
              return (
                <InputNum
                  key={key}
                  name={data.id}
                  placeholder={`${data.name} ( Max bal: ${data.maxBall} )`}
                  rules={[
                    {
                      required: true,
                    },
                  ]}
                />
              );
            })}
        </CustomCardBlock>
        <BottomActionPopup open={true}>
          <Row justify="space-between" align="middle">
            <Button loading={loading} type="primary" htmlType="submit">
              Saqlash
            </Button>
          </Row>
        </BottomActionPopup>
      </Form>
    </>
  );
}

export default AddReport1;
