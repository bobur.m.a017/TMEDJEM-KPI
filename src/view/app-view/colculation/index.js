import React, { useContext, useEffect, useState } from "react";
import { Table, Tag } from "antd";
import { TaskApi } from "../../../api/main/task";
import ColculationCard from "./components/ColculationCard";
import { MainContext } from "../../../context/MainContext";
import TableView from "../../../components/TableView";

function Colculation() {
  const [taskList, setTaskList] = useState([]);
  const [userTask, setUserTasks] = useState([]);
  const {
    setVisible,
    setEdit,
    form,
    resetFields,
    reportList,
    getReport,
    loader,
  } = useContext(MainContext);
  const currentUserId = localStorage.getItem("id");

  const handleClick = (value, data) => {
    if (parseInt(data.roleId) === parseInt(currentUserId)) {
      setVisible(true);
      form.setFieldsValue(value);
      setUserTasks(data);
    }
  };
  const onClose = () => {
    setEdit(false);
    resetFields();
    setUserTasks({});
  };
  const fetchReportData = () => {
    TaskApi.getAll()
      .then((res) => {
        setTaskList(res);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  useEffect(() => {
    getReport();
    fetchReportData();
  }, []);
  const columns = [
    {
      title: "Boshqarma",
      key: "boards",
      render: (text, value) => value.category?.name,
    },
    {
      title: "Lavozimi",
      key: "position",
      render: (text, value) =>
        value.leader ? value.leader?.professionName?.name : "-",
    },
    {
      title: "FIO",
      key: "fio",
      render: (text, value) =>
        value.leader
          ? value.leader?.name +
            " " +
            value.leader?.lastName +
            " " +
            value.leader?.fatherName
          : "-",
    },
  ];
  taskList.map((data) => {
    columns.push({
      title: data.name,
      key: data.id,
      render: (text, value) => {
        return value.categoryReport
          ? value.categoryReport?.userTasks.map((item) => {
              if (item.taskId === data.id) {
                return (
                  <Tag color={"green"}>
                    {Math.round(item.score / value.users.length)}
                  </Tag>
                );
              }
            })
          : "0";
      },
    });
  });

  reportList.map((data, id) => {
    data.key = id;
  });
  if (columns[3]) {
    columns[3].children = [
      {
        title: "Reja",
        key: 1,
        render: (_, value) => {
          return value.categoryReport?.userTasks
            .filter((data) => data.roleName === "ROLE_Nazorat")
            .map((data) => {
              return <Tag>{data.plan}</Tag>;
            });
        },
      },
      {
        title: "Bajarilgan",
        key: 2,
        render: (_, value) => {
          return value.categoryReport?.userTasks
            .filter((data) => data.roleName === "ROLE_Nazorat")
            .map((data) => {
              return <Tag>{data.implementation}</Tag>;
            });
        },
      },
      {
        title: "Bal",
        key: 3,
        render: (_, value) => {
          return value.categoryReport?.userTasks
            .filter((data) => data.roleName === "ROLE_Nazorat")
            .map((data) => {
              return (
                <Tag color={"green"}>
                  {Math.round(data.score / value.users?.length)}
                </Tag>
              );
            });
        },
      },
      {
        title: "Foiz",
        key: 4,
        render: (_, value) => {
          return value.categoryReport?.userTasks
            .filter((data) => data.roleName === "ROLE_Nazorat")
            .map((data) => {
              return <Tag color={"red"}>{data.percentage}%</Tag>;
            });
        },
      },
      {
        title: "O'rtacha plan",
        key: 5,
        render: (_, value) => {
          return value.categoryReport?.userTasks
            .filter((data) => data.roleName === "ROLE_Nazorat")
            .map((data) => {
              return (
                <Tag color="blue">
                  {Math.round(data.plan / value.users?.length)}
                </Tag>
              );
            });
        },
      },
    ];
  }
  if (currentUserId !== "4") {
    columns.push({
      title: "Jami Bal",
      key: "total_count",
      render: (text, value) => (
        <Tag color={"green"}>
          {value.categoryReport?.generalBall
            ? value.categoryReport.generalBall
            : 0}
        </Tag>
      ),
    });
  }

  columns.push({
    title: "Ustama Foiz",
    key: "extra_pracent",
    render: (text, value) => (
      <Tag color={"red"}>
        {value.categoryReport?.additionalPayment
          ? value.categoryReport?.additionalPayment
          : 0}
        %
      </Tag>
    ),
  });
  const expandedRowRender = (data) => {
    const columns = [
      {
        title: "Bo'lim",
        dataIndex: "section",
        key: "section",
        render: (_, value) =>
          value.categoryName ? value.categoryName?.name : "-",
      },
      {
        title: "Lavozimi",
        dataIndex: "position",
        key: "position",
        render: (_, value) => (value ? value?.professionName?.name : "-"),
      },
      {
        title: "FIO",
        dataIndex: "fio",
        key: "fio",
        render: (_, value) =>
          value
            ? value?.name + " " + value?.lastName + " " + value?.fatherName
            : "-",
      },
      {
        title: "",
        key: "",
      },
    ];
    data.users.map((item) => {
      item.report.userTasks.map((data, id) => {
        data.key = id;
        data.title = data.name;
        data.render = (_, value) => {
          return value.report.userTasks
            .filter(
              (data) =>
                data.roleId === parseInt(currentUserId) ||
                parseInt(currentUserId) === 1
            )
            .map((item) => {
              if (data.taskId === item.taskId) {
                return (
                  <Tag
                    color="green"
                    style={{
                      cursor: "pointer",
                    }}
                    onClick={() => handleClick(value, data)}
                  >
                    {item.score}
                  </Tag>
                );
              }
            });
        };
      });
    });
    data.users.map((item) => {
      if (parseInt(currentUserId) === 1 || parseInt(currentUserId) === 6) {
        columns[3].children = item.report?.userTasks;
      } else {
        columns[3].children = item.report?.userTasks.filter(
          (data) => data.roleId === parseInt(currentUserId)
        );
      }
    });
    columns[3].children[0].children = [
      {
        title: "Reja",
        key: 1,
        render: (_, value) => {
          return value.report.userTasks
            .filter((data) => data.roleName === "ROLE_Nazorat")
            .map((data) => {
              return (
                <Tag
                  style={{
                    cursor: "pointer",
                  }}
                  onClick={() => handleClick(value, data)}
                >
                  {data.plan}
                </Tag>
              );
            });
        },
      },
      {
        title: "Bajarilgan",
        key: 2,
        render: (_, value) => {
          return value.report.userTasks
            .filter((data) => data.roleName === "ROLE_Nazorat")
            .map((data) => {
              return (
                <Tag
                  style={{
                    cursor: "pointer",
                  }}
                  onClick={() => handleClick(value, data)}
                >
                  {data.implementation}
                </Tag>
              );
            });
        },
      },
      {
        title: "Bal",
        key: 3,
        render: (_, value) => {
          return value.report.userTasks
            .filter((data) => data.roleName === "ROLE_Nazorat")
            .map((data) => {
              return <Tag color={"green"}>{data.score}</Tag>;
            });
        },
      },
      {
        title: "Foiz",
        key: 4,
        render: (_, value) => {
          return value.report.userTasks
            .filter((data) => data.roleName === "ROLE_Nazorat")
            .map((data) => {
              return <Tag color={"red"}>{data.percentage}%</Tag>;
            });
        },
      },
    ];
    if (currentUserId !== "4") {
      columns.push({
        title: "Jami Bal",
        key: "total_count",
        render: (_, value) =>
          value.report ? (
            <Tag color={"green"}>{value.report?.generalBall}</Tag>
          ) : (
            "0"
          ),
      });
    }
    columns.push({
      title: "Ustama Foiz",
      key: "extra_pracent",
      render: (_, value) =>
        value.report ? (
          <Tag color={"red"}>{value.report?.additionalPayment} %</Tag>
        ) : (
          "0"
        ),
    });
    return (
      <Table
        columns={columns}
        dataSource={data.users}
        pagination={false}
        sticky
        className="custome-bg"
      />
    );
  };
  const date = new Date().toString();

  return (
    <div>
      <TableView
        title={`${date.slice(3, 7)}/${date.slice(10, 15)} - oy uchun hisobot`}
        className="components-table-demo-nested"
        columns={columns}
        expandable={{ expandedRowRender }}
        data={reportList}
        sticky={true}
        loading={loader}
        scroll={{ x: 1500 }}
        hidden={true}
        filter={true}
        name="colculation"
      />
      <ColculationCard
        userTask={userTask}
        taskList={taskList}
        fetchReportData={fetchReportData}
        onClose={onClose}
        title="Report"
        form={form}
      />
    </div>
  );
}

export default Colculation;
