import React from "react";
import { Route, Switch } from "react-router-dom";
import Position from "./components/position";
import Task from "./components/task";
import Admin from "./components/admin";

const AppView = () => {
  return (
    <Switch>
      <Route path={"/app/settings/position"} component={Position} />
      <Route path={"/app/settings/task"} component={Task} />
      <Route path={"/app/settings/admin"} component={Admin} />
      {/* <Route
          path={"/app/settings/percent"}
          component={lazy(() => import("./components/Percent"))}
        />
        <Route
          path={"/app/settings/permission"}
          component={lazy(() => import("./components/Permission"))}
        /> */}
    </Switch>
  );
};

export default AppView;
