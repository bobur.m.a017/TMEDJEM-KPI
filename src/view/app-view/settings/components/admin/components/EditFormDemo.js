import { Button, Form, Row, Col, message } from "antd";
import React, { useContext, useEffect, useState } from "react";
import CustomCardBlock from "../../../../../../components/Card/CustomCardBlock";
import Spinner from "../../../../../../components/Loading/Spinner";
import BottomActionPopup from "../../../../../../components/util-component/BottomActionPopup";
import InputText from "../../../../../../components/util-component/Form/InputText";
import InputPassword from "../../../../../../components/util-component/Form/InputPassword";
import InputSelect from "../../../../../../components/util-component/Form/Select";
import { MainContext } from "../../../../../../context/MainContext";
import { UserApi } from "../../../../../../api/main/user";
import { RoleApi } from "../../../../../../api/main/role";

function AddFormDemo({ title }) {
  const { getUser, form, resetFields } = useContext(MainContext);
  const [sending, setSending] = useState(false);
  const [role, setRole] = useState([]);

  const onFinish = (values) => {
    values.id = form.getFieldValue().id;
    setSending(true);
    UserApi.admin(values)
      .then(() => {
        resetFields();
        message.success("Success");
        setSending(false);
        getUser();
      })
      .catch((error) => {
        console.log(error);
        message.error("Error");
        setSending(false);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  useEffect(() => {
    RoleApi.getAll()
      .then((data) => {
        setRole(data);
      })
      .catch((error) => console.log(error));
  }, []);
  return (
    <>
      {sending && <Spinner />}
      <Form
        form={form}
        initialValues={{}}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <CustomCardBlock title={title}>
          <Row gutter={24}>
            <Col span={20} offset={2}>
              <InputText
                name="username"
                placeholder="Foydalanuchni nomi"
                rules={[
                  {
                    required: true,
                  },
                ]}
              />
            </Col>
            <Col span={20} offset={2}>
              <InputPassword
                name="password"
                placeholder="Parol"
                rules={[
                  {
                    required: true,
                  },
                ]}
              />
            </Col>
            <Col span={20} offset={2}>
              <InputSelect
                name="roleId"
                placeholder="Rolni tanlang"
                options={role}
              />
            </Col>
          </Row>
        </CustomCardBlock>
        <BottomActionPopup open={true}>
          <Row justify="space-between" align="middle">
            <Button
              loading={sending}
              type="primary"
              onClick={resetFields}
              htmlType="reset"
            >
              Bekor qilish
            </Button>
            <Button loading={sending} type="primary" htmlType="submit">
              Saqlash
            </Button>
          </Row>
        </BottomActionPopup>
      </Form>
    </>
  );
}

export default AddFormDemo;
