import React, { useContext, useEffect } from "react";
import TableView from "../../../../../components/TableView";
import { MainContext } from "../../../../../context/MainContext";
import StaffCard from "./components/StaffCard";
function Staff() {
  const {
    setVisible,
    setEdit,
    getUser,
    userList,
    form,
    resetFields,
    setCategorySub,
    setManagementSub,
    loader,
  } = useContext(MainContext);

  const onClose = () => {
    setEdit(false);
    resetFields();
    setCategorySub([]);
    setManagementSub([]);
  };
  const handleChange = (value) => {
    form.setFieldsValue({ ...value });
    setEdit(true);
    setVisible(true);
  };

  useEffect(() => {
    getUser();
  }, []);
  let i = 1;
  const columns = [
    {
      title: "T/R",
      dataIndex: "id",
      key: "id",
      render: (text, value) => userList.indexOf(value) + 1,
    },
    {
      title: "Ism",
      dataIndex: "name",
      key: "name",
      render: (text, value) => (
        <a onClick={() => handleChange(value)}>{text}</a>
      ),
    },
    {
      title: "Familya",
      dataIndex: "lastName",
      key: "lastName",
    },
    {
      title: "Otasining ismi",
      dataIndex: "fatherName",
      key: "fatherName",
    },
    {
      title: "JSH SHIR",
      dataIndex: "jshshir",
      key: "jshshir",
    },
    {
      title: "Lavozim",
      key: "professionName",
      render: (text, record) => record.professionName?.name,
    },
    {
      title: "Holati",
      dataIndex: "status",
      key: "status",
    },
    {
      title: "Bo'lim",
      key: "categoryName",
      render: (text, record) => record.categoryName?.name,
    },
  ];
  return (
    <div>
      <TableView
        title="Xodimlarni shakllantirish!"
        columns={columns}
        data={userList}
        loading={loader}
        filter={true}
        sticky
        hidden={true}
      />
      <StaffCard onClose={onClose} title="Rol biriktirish" />
    </div>
  );
}

export default Staff;
