import React, { useContext, useEffect, useState } from "react";
import { Drawer, message } from "antd";
import EditForm from "./EditForm";
import AddFrom from "./AddFrom";
import { MainContext } from "../../../../../../context/MainContext";
import { RoleApi } from "../../../../../../api/main/role";

function TaskCard({ onClose, getAllTask }) {
  const { edit, visible } = useContext(MainContext);
  const [role, SetRole] = useState([]);

  useEffect(() => {
    RoleApi.getAll()
      .then((res) => {
        SetRole(res);
      })
      .catch((error) => {
        message.error("Error");
        console.log(error);
      });
  }, []);
  return (
    <Drawer
      title={`kpi.edu.uz`}
      placement="right"
      size="large"
      onClose={onClose}
      visible={visible}
    >
      {edit && <EditForm role={role} getAllTask={getAllTask} />}
      {!edit && <AddFrom role={role} getAllTask={getAllTask} />}
    </Drawer>
  );
}

export default TaskCard;
