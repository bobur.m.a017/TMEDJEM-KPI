import { Button, message, Row, Switch } from "antd";
import { Form } from "antd";
import React, { useContext } from "react";
import { TaskApi } from "../../../../../../api/main/task";
import CustomCardBlock from "../../../../../../components/Card/CustomCardBlock";
import BottomActionPopup from "../../../../../../components/util-component/BottomActionPopup";
import InputText from "../../../../../../components/util-component/Form/InputText";
import InputSelect from "../../../../../../components/util-component/Form/Select";
import { MainContext } from "../../../../../../context/MainContext";

function EditForm({ role, getAllTask }) {
  const { form, resetFields } = useContext(MainContext);

  const onFinish = (values) => {
    TaskApi.update(form.getFieldValue().id, values)
      .then((res) => {
        resetFields();
        message.success(res.text);
        getAllTask();
      })
      .catch((error) => {
        message.error("Error");
        console.log(error);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <>
      <Form
        form={form}
        initialValues={{}}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <CustomCardBlock title="Topshiriq Tahrirlash">
          <InputText
            name="name"
            placeholder="Topshiriq nomi"
            rules={[
              {
                required: true,
              },
            ]}
          />
          <InputText
            name="minBall"
            placeholder="Minimal Bal"
            rules={[
              {
                required: true,
              },
            ]}
          />
          <InputText
            name="maxBall"
            placeholder="Maksimalk Bal"
            rules={[
              {
                required: true,
              },
            ]}
          />
          <InputSelect
            name="roleId"
            placeholder="Rolni tanlang"
            options={role}
            rules={[
              {
                required: true,
              },
            ]}
          />
          <Form.Item name="type" rules={[{ required: true }]}>
            <Switch checkedChildren="Foiz" unCheckedChildren="Bal" />
          </Form.Item>
        </CustomCardBlock>
        <BottomActionPopup open={true}>
          <Row justify="space-between" align="middle">
            <Button type="primary" htmlType="reset" onClick={resetFields}>
              Bekor qilish
            </Button>
            <Button type="primary" htmlType="submit">
              Saqlash
            </Button>
          </Row>
        </BottomActionPopup>
      </Form>
    </>
  );
}

export default EditForm;
