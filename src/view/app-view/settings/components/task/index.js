import React, { useContext, useEffect, useState } from "react";
import TableView from "../../../../../components/TableView";
import { MainContext } from "../../../../../context/MainContext";
import { DeleteOutlined } from "@ant-design/icons";
import { Popconfirm, Tag, message } from "antd";
import TaskCard from "./components/TaskCard";
import { TaskApi } from "../../../../../api/main/task";

function Task() {
  const { setVisible, setEdit, form, resetFields, loader } =
    useContext(MainContext);
  const [tasks, setTasks] = useState([]);
  const Delete = (id) => {
    TaskApi.delete(id)
      .then((res) => {
        message.success(res.text);
        getAllTask();
      })
      .catch((err) => {
        message.error("error");
        console.log(err);
      });
  };
  const onClose = () => {
    setEdit(false);
    resetFields();
  };
  const handleChange = (value) => {
    form.setFieldsValue({ ...value });
    setEdit(true);
    setVisible(true);
  };
  const getAllTask = () => {
    TaskApi.getAll()
      .then((res) => {
        setTasks(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    getAllTask();
  }, []);
  const columns = [
    { title: "ID", dataIndex: "id", key: "id" },
    {
      title: "Nomi",
      dataIndex: "name",
      key: "name",
      render: (text, value) => (
        <a
          onClick={() => {
            handleChange(value);
          }}
        >
          {text}
        </a>
      ),
    },
    {
      title: "Minimal Bal",
      dataIndex: "minBall",
      key: "minBall",
    },
    {
      title: "Maksimal Bal",
      dataIndex: "maxBall",
      key: "maxBall",
    },
    {
      title: "Rol",
      dataIndex: "role",
      key: "role",
      render: (text, value) => value.type,
    },
    {
      title: "Turi",
      dataIndex: "type",
      key: "type",
      render: (text, value) => (value.type ? "Foiz" : "Bal"),
    },
    {
      title: "O'chirish",
      key: 0,
      render: (_, record) => (
        <Popconfirm title="Sure to delete?" onConfirm={() => Delete(record.id)}>
          <Tag color="red" key={0}>
            <DeleteOutlined />
          </Tag>
        </Popconfirm>
      ),
    },
  ];

  return (
    <div>
      <TableView
        title="Topshiriq shakllantirish!"
        data={tasks}
        loading={loader}
        columns={columns}
        setVisible={setVisible}
      />
      <TaskCard getAllTask={getAllTask} onClose={onClose} />
    </div>
  );
}

export default Task;
