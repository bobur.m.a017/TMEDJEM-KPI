import { Button, message, Row } from "antd";
import { Form } from "antd";
import React, { useContext } from "react";
import { PositionApi } from "../../../../../../api/main/position";
import CustomCardBlock from "../../../../../../components/Card/CustomCardBlock";
import BottomActionPopup from "../../../../../../components/util-component/BottomActionPopup";
import InputText from "../../../../../../components/util-component/Form/InputText";
import { MainContext } from "../../../../../../context/MainContext";

function EditForm() {
  const { form, resetFields, dataFetch } = useContext(MainContext);

  const onFinish = (values) => {
    PositionApi.update(form.getFieldValue().id, values)
      .then((res) => {
        resetFields();
        message.success(res.text);
        dataFetch();
      })
      .catch((error) => {
        message.error("Error");
        console.log(error);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <>
      <Form
        form={form}
        initialValues={{}}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <CustomCardBlock title="Lavozim Tahrirlash">
          <InputText
            name="name"
            placeholder="Lavozim nomi"
            rules={[
              {
                required: true,
              },
            ]}
          />
        </CustomCardBlock>
        <BottomActionPopup open={true}>
          <Row justify="space-between" align="middle">
            <Button type="primary" htmlType="reset" onClick={resetFields}>
              Bekor qilish
            </Button>
            <Button type="primary" htmlType="submit">
              Saqlash
            </Button>
          </Row>
        </BottomActionPopup>
      </Form>
    </>
  );
}

export default EditForm;
