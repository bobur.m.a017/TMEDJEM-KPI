import React, { useContext } from "react";
import { Drawer } from "antd";
import EditForm from "./EditForm";
import AddFrom from "./AddFrom";
import { MainContext } from "../../../../../../context/MainContext";

function PositionCard({ onClose }) {
  const { edit, visible } = useContext(MainContext);

  return (
    <Drawer
      title={`kpi.edu.uz`}
      placement="right"
      size="large"
      onClose={onClose}
      visible={visible}
    >
      {edit && <EditForm />}
      {!edit && <AddFrom />}
    </Drawer>
  );
}

export default PositionCard;
