import React, { useContext } from "react";
import TableView from "../../../../../components/TableView";
import { MainContext } from "../../../../../context/MainContext";
import { DeleteOutlined } from "@ant-design/icons";
import { Popconfirm, Tag, message } from "antd";
import PositionCard from "./components/PositionCard";
import { PositionApi } from "../../../../../api/main/position";

function Position() {
  const { setVisible, setEdit, form, position, dataFetch, loader } =
    useContext(MainContext);

  const Delete = (id) => {
    PositionApi.delete(id)
      .then((res) => {
        dataFetch();
        message.success(res.text);
      })
      .catch((err) => {
        message.error("error");
        console.log(err);
      });
  };
  const onClose = () => {
    setVisible(false);
    setEdit(false);
    form.resetFields();
  };
  const handleChange = (value) => {
    form.setFieldsValue({ ...value });
    setEdit(true);
    setVisible(true);
  };

  const columns = [
    { title: "ID", dataIndex: "id", key: "id" },
    {
      title: "Nomi",
      dataIndex: "name",
      key: "name",
      render: (text, value) => (
        <a
          onClick={() => {
            handleChange(value);
          }}
        >
          {text}
        </a>
      ),
    },
    {
      title: "O'chirish",
      key: 0,
      render: (_, record) => (
        <Popconfirm title="Sure to delete?" onConfirm={() => Delete(record.id)}>
          <Tag color="red" key={0}>
            <DeleteOutlined />
          </Tag>
        </Popconfirm>
      ),
    },
  ];

  return (
    <div>
      <TableView
        title="Lavozim shakllantirish!"
        data={position}
        loading={loader}
        columns={columns}
        setVisible={setVisible}
      />
      <PositionCard onClose={onClose} />
    </div>
  );
}

export default Position;
