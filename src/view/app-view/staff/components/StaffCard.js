import React, { useContext } from "react";
import { Drawer } from "antd";
import { MainContext } from "../../../../context/MainContext";
import AddFormDemo from "./AddFormDemo";
import EditFormDemo from "./EditFormDemo";

function StaffCard({ onClose, title = "" }) {
  const { edit, visible } = useContext(MainContext);

  return (
    <Drawer
      title={`kpi.edu.uz`}
      placement="right"
      size="large"
      onClose={onClose}
      visible={visible}
    >
      {edit && <EditFormDemo title={title} />}
      {!edit && <AddFormDemo title={title} />}
    </Drawer>
  );
}
export default StaffCard;
