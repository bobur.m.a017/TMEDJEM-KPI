import React, { useContext } from "react";
import InputText from "../../../../components/util-component/Form/InputText";
import InputSelect from "../../../../components/util-component/Form/Select";
import { Button, Col, Form, Row, Tooltip } from "antd";
import { SearchOutlined } from "@ant-design/icons";
import { CategoryOptions } from "../../../../constants";
import { MainContext } from "../../../../context/MainContext";

function FilterStuff({ setActive, active }) {
  const { getUser } = useContext(MainContext);

  const [form] = Form.useForm();
  const onFinish = (values) => {
    getUser(values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const resetEvent = () => {
    form.resetFields();
    setActive(false);
    getUser();
  };
  return (
    <div className={active ? "custom-collapse active" : "custom-collapse"}>
      <Form
        form={form}
        name="basic"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Row gutter={24}>
          <Col span={24}>
            <InputText
              onFocus={() => setActive(!active)}
              name="search"
              className="search-input"
              suffix={
                <Tooltip title="Search">
                  <SearchOutlined />
                </Tooltip>
              }
              placeholder="Search & Filter"
            />
          </Col>
          <Col span={22} offset={1}>
            <InputSelect
              name="filter"
              placeholder="Select Category"
              options={CategoryOptions}
            />
          </Col>
          <Col span={22} offset={1}>
            <InputText name="jshshir" placeholder="JSH SHIR" />
          </Col>
          <Col
            span={22}
            offset={1}
            style={{ display: "flex", justifyContent: "space-between" }}
          >
            <Form.Item>
              <Button type="primary" htmlType="reset" onClick={resetEvent}>
                Bekor qilish
              </Button>
            </Form.Item>
            <Form.Item>
              <Button loading={!active} type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </div>
  );
}

export default FilterStuff;
