import { Button, Form, Row, Col, message } from "antd";
import React, { useContext, useEffect, useState } from "react";
import { CategoryApi } from "../../../../api/main/category";
import CustomCardBlock from "../../../../components/Card/CustomCardBlock";
import Spinner from "../../../../components/Loading/Spinner";
import BottomActionPopup from "../../../../components/util-component/BottomActionPopup";
import InputText from "../../../../components/util-component/Form/InputText";
import InputSelect from "../../../../components/util-component/Form/Select";
import { StuffStatus } from "../../../../constants";
import { MainContext } from "../../../../context/MainContext";
import { UserApi } from "../../../../api/main/user";

function AddFormDemo({ title }) {
  const {
    position,
    category,
    getUser,
    form,
    resetFields,
    categorySub,
    setCategorySub,
    managemenSub,
    setManagementSub,
  } = useContext(MainContext);

  const [sending, setSending] = useState(false);

  const onFinish = (values) => {
    setSending(true);
    UserApi.update(form.getFieldValue().id, values)
      .then(() => {
        resetFields();
        message.success("Success");
        setSending(false);
        getUser();
      })
      .catch((error) => {
        console.log(error);
        message.error("Error");
        setSending(false);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const getSubCategory = (type, id) => {
    CategoryApi.getSub(id).then((response) => {
      if (type === "managerId") {
        setManagementSub(response);
      } else {
        setCategorySub(response);
      }
    });
  };

  useEffect(() => {
    setManagementSub(category?.filter((data) => data.typeName === "Boshqarma"));
    setCategorySub(category?.filter((data) => data.typeName === "Bo'lim"));
  }, []);
  const handleChangedField = (field) => {
    if (field[0]?.name[0] === "boardId") {
      getSubCategory(field[0]?.name[0], field[0].value);
    }
    if (field[0]?.name[0] === "managerId") {
      getSubCategory(field[0]?.name[0], field[0].value);
    }
  };
  if (categorySub[0]?.value !== null) {
    categorySub.unshift({ value: null, name: "Boshqarmaga O'tkazish" });
  }
  if (managemenSub[0]?.value !== null) {
    managemenSub.unshift({ value: null, name: "Rahbariyatga o'tkazish" });
  }
  return (
    <>
      {sending && <Spinner />}
      <Form
        form={form}
        initialValues={{
          professionId: form.getFieldValue().professionName?.id,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        onFieldsChange={(changedFields) => {
          handleChangedField(changedFields);
        }}
      >
        <CustomCardBlock title={title}>
          <Row gutter={24}>
            <Col span={12}>
              <InputText
                name="name"
                placeholder="Ism"
                rules={[
                  {
                    required: true,
                  },
                ]}
              />
            </Col>
            <Col span={12}>
              <InputText
                name="lastName"
                placeholder="Familya"
                rules={[
                  {
                    required: true,
                  },
                ]}
              />
            </Col>
            <Col span={12}>
              <InputText
                name="fatherName"
                placeholder="Otasining Ismi"
                rules={[
                  {
                    required: true,
                  },
                ]}
              />
            </Col>
            <Col span={12}>
              <InputText
                name="jshshir"
                placeholder="JSH SHIR"
                rules={[
                  {
                    required: true,
                  },
                ]}
              />
            </Col>
            <Col span={12}>
              <InputSelect
                name="managerId"
                placeholder="Rahbariyatni tanlang"
                options={category?.filter(
                  (data) => data.typeName === "Rahbariyat"
                )}
                rules={[
                  {
                    required: true,
                  },
                ]}
              />
            </Col>
            <Col span={12}>
              <InputSelect
                name="boardId"
                placeholder="Boshqarmani tanlang"
                disabled={!(managemenSub.length - 1)}
                options={managemenSub}
              />
            </Col>
            <Col span={12}>
              <InputSelect
                name="sectionId"
                placeholder="Bo'limni tanlang"
                disabled={!(categorySub.length - 1)}
                options={categorySub}
              />
            </Col>
            <Col span={12}>
              <InputSelect
                name="professionId"
                placeholder="Lavozimni tanlang"
                options={position}
                rules={[
                  {
                    required: true,
                  },
                ]}
              />
            </Col>
            <Col span={12}>
              <InputSelect
                name="status"
                placeholder="Holatni tanlang"
                options={StuffStatus}
                rules={[
                  {
                    required: true,
                  },
                ]}
              />
            </Col>
          </Row>
        </CustomCardBlock>
        <BottomActionPopup open={true}>
          <Row justify="space-between" align="middle">
            <Button
              loading={sending}
              type="primary"
              onClick={resetFields}
              htmlType="reset"
            >
              Bekor qilish
            </Button>
            <Button loading={sending} type="primary" htmlType="submit">
              Saqlash
            </Button>
          </Row>
        </BottomActionPopup>
      </Form>
    </>
  );
}

export default AddFormDemo;
