import React, { useState } from "react";
import { Tabs } from "antd";
import ManagerStuff from "./addform/Manager";
import BoardsStuff from "./addform/Boards";
import SectionStuff from "./addform/Section";

const { TabPane } = Tabs;

function AddFormTab({ title }) {
  return (
    <Tabs defaultActiveKey="1" centered>
      <TabPane tab="Rahbariyat" key="1">
        <ManagerStuff title={title} />
      </TabPane>
      <TabPane tab="Boshqarma" key="2">
        <BoardsStuff title={title} />
      </TabPane>
      <TabPane tab="Bo'lim" key="3">
        <SectionStuff title={title} />
      </TabPane>
    </Tabs>
  );
}

export default AddFormTab;
