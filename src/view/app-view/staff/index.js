import React, { useContext, useEffect } from "react";
import TableView from "../../../components/TableView";
import { DeleteOutlined } from "@ant-design/icons";
import { message, Popconfirm, Space, Tag } from "antd";
import { MainContext } from "../../../context/MainContext";
import StaffCard from "./components/StaffCard";
import { UserApi } from "../../../api/main/user";
function Staff() {
  const {
    setVisible,
    setEdit,
    getUser,
    userList,
    form,
    resetFields,
    setCategorySub,
    setManagementSub,
    loader,
  } = useContext(MainContext);

  const Delete = (id) => {
    UserApi.delete(id)
      .then((res) => {
        message.success(res.text);
        getUser();
      })
      .catch((err) => {
        message.error("error");
        console.log(err);
      });
  };
  const onClose = () => {
    setEdit(false);
    resetFields();
    setCategorySub([]);
    setManagementSub([]);
  };
  const handleChange = (value) => {
    form.setFieldsValue({ ...value });
    setEdit(true);
    setVisible(true);
  };

  useEffect(() => {
    getUser();
  }, []);
  let i = 1;
  const columns = [
    {
      title: "T/R",
      dataIndex: "id",
      key: "id",
      render: (text, value) => userList.indexOf(value) + 1,
    },
    {
      title: "Ism",
      dataIndex: "name",
      key: "name",
      render: (text, value) => (
        <a onClick={() => handleChange(value)}>{text}</a>
      ),
    },
    {
      title: "Familya",
      dataIndex: "lastName",
      key: "lastName",
    },
    {
      title: "Otasining ismi",
      dataIndex: "fatherName",
      key: "fatherName",
    },
    {
      title: "JSH SHIR",
      dataIndex: "jshshir",
      key: "jshshir",
    },
    {
      title: "Lavozim",
      key: "professionName",
      render: (text, record) => record.professionName?.name,
    },
    {
      title: "Holati",
      dataIndex: "status",
      key: "status",
    },
    {
      title: "Bo'lim",
      key: "categoryName",
      render: (text, record) => record.categoryName?.name,
    },
    {
      title: "O'chirish",
      key: 0,
      render: (_, record) => (
        <Popconfirm title="Sure to delete?" onConfirm={() => Delete(record.id)}>
          <Tag color="red" key={0}>
            <DeleteOutlined />
          </Tag>
        </Popconfirm>
      ),
    },
  ];
  return (
    <div>
      <TableView
        title="Xodimlarni shakllantirish!"
        columns={columns}
        data={userList}
        filter={true}
        loading={loader}
        name="staff"
      />
      <StaffCard onClose={onClose} title="Xodim qo'shish" />
    </div>
  );
}

export default Staff;
