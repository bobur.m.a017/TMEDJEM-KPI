import { Button, message, Row } from "antd";
import { Form } from "antd";
import React, { useContext } from "react";
import { CategoryApi } from "../../../../api/main/category";
import CustomCardBlock from "../../../../components/Card/CustomCardBlock";
import BottomActionPopup from "../../../../components/util-component/BottomActionPopup";
import InputText from "../../../../components/util-component/Form/InputText";
import { MainContext } from "../../../../context/MainContext";

function AddFrom() {
  const { dataFetch, setVisible, form } = useContext(MainContext);

  const onFinish = (values) => {
    CategoryApi.create(values)
      .then((res) => {
        dataFetch();
        setVisible(false);
        form.resetFields();
        message.success(res.text);
      })
      .catch((error) => {
        message.error("Error");
        console.log(error);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <>
      <Form
        form={form}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <CustomCardBlock title="Rahbariyat Qo'shish">
          <InputText
            name="name"
            placeholder="Rahbariyat nomi"
            rules={[
              {
                required: true,
              },
            ]}
          />
        </CustomCardBlock>
        <BottomActionPopup open={true}>
          <Row justify="space-between" align="middle">
            <Button type="primary" htmlType="submit">
              Saqlash
            </Button>
          </Row>
        </BottomActionPopup>
      </Form>
    </>
  );
}

export default AddFrom;
