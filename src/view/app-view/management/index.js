import React, { useContext } from "react";
import TableView from "../../../components/TableView";
import { MainContext } from "../../../context/MainContext";
import ManagementCard from "./components/ManagementCard";
import { DeleteOutlined } from "@ant-design/icons";
import { Popconfirm, Tag, message } from "antd";
import { CategoryApi } from "../../../api/main/category";

function Management() {
  const { setVisible, setEdit, category, dataFetch, form, loader } =
    useContext(MainContext);
  const data = category?.filter((data) => data.typeName === "Rahbariyat");
  const Delete = (id) => {
    CategoryApi.delete(id)
      .then((res) => {
        dataFetch();
        message.success(res.text);
      })
      .catch((err) => {
        message.error("error");
        console.log(err);
      });
  };
  const onClose = () => {
    setVisible(false);
    setEdit(false);
    form.resetFields();
  };

  const handleChange = (value) => {
    form.setFieldsValue({ ...value });
    setEdit(true);
    setVisible(true);
  };
  const columns = [
    {
      title: "T/R",
      dataIndex: "id",
      key: "id",
      render: (_, value) => data.indexOf(value) + 1,
    },
    {
      title: "Nomi",
      dataIndex: "name",
      key: "name",
      render: (text, value) => (
        <a
          // style={{ color: "#1890ff" }}
          onClick={() => {
            handleChange(value);
          }}
        >
          {text}
        </a>
      ),
    },
    {
      title: "O'chirish",
      key: 0,
      render: (_, record) => (
        <Popconfirm title="Sure to delete?" onConfirm={() => Delete(record.id)}>
          <Tag color="red" key={0}>
            <DeleteOutlined />
          </Tag>
        </Popconfirm>
      ),
    },
  ];

  return (
    <div>
      <TableView
        title="Rahbariyatni shakllantirish!"
        data={data}
        columns={columns}
        setVisible={setVisible}
        loading={loader}
      />
      <ManagementCard onClose={onClose} />
    </div>
  );
}

export default Management;
