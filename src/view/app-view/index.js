import React from "react";
import { Route, Switch } from "react-router-dom";
import MainContextProvider from "../../context/MainContext";
import Dashboard from "./dashboard";
import Boards from "./boards";
import Colculation from "./colculation";
import Management from "./management";
import Sections from "./sections";
import Settings from "./settings";
import Staff from "./staff";

const AppView = () => {
  return (
    <MainContextProvider>
      <Switch>
        <Route exact path={"/app"} component={Dashboard} />
        <Route path={"/app/boards"} component={Boards} />
        <Route path={"/app/colculation"} component={Colculation} />
        <Route path={"/app/management"} component={Management} />
        <Route path={"/app/sections"} component={Sections} />
        <Route path={"/app/settings"} component={Settings} />
        <Route path={"/app/staff"} component={Staff} />
      </Switch>
    </MainContextProvider>
  );
};

export default AppView;
