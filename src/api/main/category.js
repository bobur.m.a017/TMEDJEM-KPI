import mainCaller from "./mainCaller";
import HTTPMethods from "../HTTPMethods";

export class CategoryApi {
  static endpoint = "/kpi/api/categories";
  static getAll() {
    return mainCaller(this.endpoint, HTTPMethods.GET);
  }
  static create(data) {
    return mainCaller(this.endpoint, HTTPMethods.POST, data);
  }
  static delete(id) {
    return mainCaller(this.endpoint + "/" + id, HTTPMethods.DELETE);
  }
  static update(id, data) {
    return mainCaller(this.endpoint + "/" + id, HTTPMethods.PUT, data);
  }

  static getSub(id) {
    return mainCaller(this.endpoint + "/getSub/" + id);
  }
    static getParent(id) {
    return mainCaller(this.endpoint + "/getParent/" + id);
  }
}
