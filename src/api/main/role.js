import mainCaller from "./mainCaller";
import HTTPMethods from "../HTTPMethods";

export class RoleApi {
  static endpoint = "/kpi/api/role";
  static getAll() {
    return mainCaller(this.endpoint, HTTPMethods.GET);
  }
}
