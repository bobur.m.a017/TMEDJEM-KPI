import mainCaller from "./mainCaller";
import HTTPMethods from "../HTTPMethods";

export class PositionApi {
  static endpoint = "/kpi/api/profession";
  static getAll() {
    return mainCaller(this.endpoint, HTTPMethods.GET);
  }
  static create(data) {
    return mainCaller(this.endpoint, HTTPMethods.POST, data);
  }
  static delete(id) {
    return mainCaller(this.endpoint + "/" + id, HTTPMethods.DELETE);
  }
  static update(id, data) {
    return mainCaller(this.endpoint + "/" + id, HTTPMethods.PUT, data);
  }

  static getOne(id) {
    return mainCaller(this.endpoint + "/" + id);
  }
}
