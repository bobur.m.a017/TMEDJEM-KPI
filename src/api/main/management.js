import mainCaller from "./mainCaller";
import HTTPMethods from "../HTTPMethods";

export class ManagementApi {
  static endpoint = "/kpi/api/user";

  static getAll() {
    return mainCaller(this.endpoint, HTTPMethods.GET);
  }
  static create(data) {
    return mainCaller(this.endpoint, HTTPMethods.POST, data);
  }

  static update(id, data) {
    return mainCaller(this.endpoint + "/" + id, HTTPMethods.PUT, data);
  }

  static getOne(id) {
    return mainCaller(this.endpoint + "/" + id);
  }
}
