import mainCaller from "./mainCaller";
import HTTPMethods from "../HTTPMethods";

export class UserApi {
  static endpoint = "/kpi/api/user";

  static getAll(params) {
    return mainCaller(
      this.endpoint + `/getUser`,
      HTTPMethods.GET,
      null,
      null,
      params
    );
  }
  static create(data) {
    return mainCaller(this.endpoint, HTTPMethods.POST, data);
  }

  static update(id, data) {
    return mainCaller(this.endpoint + "/" + id, HTTPMethods.PUT, data);
  }

  static delete(id) {
    return mainCaller(this.endpoint + "/" + id, HTTPMethods.DELETE);
  }
  static admin() {
    return mainCaller(this.endpoint + "/signUp", HTTPMethods.POST);
  }
}
