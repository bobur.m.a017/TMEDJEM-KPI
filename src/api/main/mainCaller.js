import HTTPMethods from "../HTTPMethods";
import axios from "axios";

export const mainUrl = "https://kpi.edu.uz";
// const mainUrl = "http://192.168.58.8:2266";
export default function mainCaller(
  path,
  method = HTTPMethods.GET,
  data,
  headers,
  params
) {
  const _headers = {
    Accept: "application/json; charset=utf-8",
    ...headers,
  };
  const options = {
    method,
    url: mainUrl + path,
  };
  options.params = params;
  if (localStorage.getItem("token"))
    _headers.Authorization = localStorage.getItem("token");

  if (data) {
    options.data = data;
    if (data instanceof FormData) {
      _headers["Content-type"] = "multipart/form-data";
    } else {
      _headers["Content-type"] = "application/json; charset=utf-8";
    }
  }

  options.headers = _headers;

  return axios(options).then((r) => r.data);
}
