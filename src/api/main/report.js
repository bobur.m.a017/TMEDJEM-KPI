import mainCaller from "./mainCaller";
import HTTPMethods from "../HTTPMethods";

export class ReportApi {
  static endpoint = "/kpi/api/report";
  static getAll(params) {
    return mainCaller(this.endpoint, HTTPMethods.GET, null, null, params);
  }
  static create(data) {
    return mainCaller(this.endpoint, HTTPMethods.POST, data);
  }
  static delete(id) {
    return mainCaller(this.endpoint + "/" + id, HTTPMethods.DELETE);
  }
  static update(id, data) {
    return mainCaller(this.endpoint + "/" + id, HTTPMethods.PUT, data);
  }

  static getSub(id) {
    return mainCaller(this.endpoint + "/getSub/" + id);
  }
}
