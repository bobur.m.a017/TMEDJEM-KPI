import React, { useState } from "react";
import { Layout, Menu, Dropdown, Avatar } from "antd";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  CalculatorOutlined,
  SettingOutlined,
  TeamOutlined,
  DatabaseOutlined,
  WalletOutlined,
  LoginOutlined,
} from "@ant-design/icons";
import AppView from "../view/app-view";
import { Link } from "react-router-dom";
import { Scrollbars } from "react-custom-scrollbars";

const { SubMenu } = Menu;

const { Header, Sider, Content } = Layout;

const MainLayout = () => {
  const [collapsed, setCollapsed] = useState(false);
  const logOut = (e) => {
    e.preventDefault();
    localStorage.removeItem("token");
    localStorage.removeItem("role");
    localStorage.removeItem("id");
  };
  const toggle = () => {
    setCollapsed(!collapsed);
  };
  const name = localStorage.getItem("name");
  const lastName = localStorage.getItem("lastName");
  const menu = (
    <Menu className="custom-drop">
      <Menu.Item key="0">
        <Avatar className="avatar" size="small" icon={<UserOutlined />} />{" "}
        {name ? (name.slice(0.1), lastName) : "Admin"}
      </Menu.Item>
      <Menu.Item key="1">
        <Link to="/">
          <LoginOutlined /> Chiqish
        </Link>
      </Menu.Item>
    </Menu>
  );
  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <Scrollbars autoHide>
          <div className={!collapsed ? "side-logo" : "d-center"}>
            {collapsed && (
              <MenuUnfoldOutlined className="toggle-button" onClick={toggle} />
            )}
            {!collapsed && (
              <MenuFoldOutlined className="toggle-button" onClick={toggle} />
            )}
            <div className={collapsed ? "d-none" : "logo"}></div>
          </div>
          <Menu mode="inline">
            {/* <Menu.Item key="0" icon={<AppstoreOutlined />}>
              <Link to="/app">Dashboard</Link>
            </Menu.Item> */}
            <Menu.Item key="1" icon={<UserOutlined />}>
              <Link to="/app/management">Rahbariyat</Link>
            </Menu.Item>
            <Menu.Item key="2" icon={<WalletOutlined />}>
              <Link to="/app/boards"> Boshqarmalar</Link>
            </Menu.Item>
            <Menu.Item key="3" icon={<DatabaseOutlined />}>
              <Link to="/app/sections"> Bo'limlar</Link>
            </Menu.Item>
            <Menu.Item key="4" icon={<TeamOutlined />}>
              <Link to="/app/staff">Hodimlar</Link>
            </Menu.Item>
            <Menu.Item key="5" icon={<CalculatorOutlined />}>
              <Link to="/app/colculation">Hisobotlar</Link>
            </Menu.Item>
            {/* <Menu.Item key="6" icon={<BarChartOutlined />}>
              <Link to="/app/reports">Hisobotlar</Link>
            </Menu.Item> */}
            <SubMenu key="sub1" icon={<SettingOutlined />} title="Sozlamalar">
              <Menu.Item key="7">
                <Link to="/app/settings/position">Lavozim</Link>
              </Menu.Item>
              <Menu.Item key="8">
                <Link to="/app/settings/task">Topshiriq</Link>
              </Menu.Item>
              <Menu.Item key="9">
                <Link to="/app/settings/admin">Admin</Link>
              </Menu.Item>
              {/* <Menu.Item key="10">
                <Link to="/app/settings/percent">Ustama</Link>
              </Menu.Item>
              <Menu.Item key="11">
                <Link to="/app/settings/permission">Ruxsat va Taqiq</Link>
              </Menu.Item> */}
            </SubMenu>
          </Menu>
        </Scrollbars>
      </Sider>
      <Layout>
        <Header className="custom-header">
          <Dropdown overlay={menu} trigger={["click"]}>
            <a className="ant-dropdown-link" onClick={(e) => logOut(e)}>
              <Avatar className="avatar" size="large" icon={<UserOutlined />} />
            </a>
          </Dropdown>
        </Header>
        <div className="app-content">
          <Scrollbars autoHide>
            <Content>
              <AppView />
            </Content>
          </Scrollbars>
        </div>
      </Layout>
    </Layout>
  );
};

export default MainLayout;
